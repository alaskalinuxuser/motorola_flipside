#!/system/bin/sh


export PATH=/sbin:/system/xbin:/system/bin:system/bootmenu/binary


#### begin alias command ############################################

export_cmd()
{
  ALIAS=$1
  COMMAND=$2

  if [ -x /sbin/$COMMAND ]; then
    export $ALIAS="/sbin/$COMMAND"
  else if [ -x /system/xbin/$COMMAND ]; then
    export $ALIAS="/system/xbin/$COMMAND"
  else if [ -x /system/bin/$COMMAND ]; then
    export $ALIAS="/system/bin/$COMMAND"
  else if [ -x /system/xbin/busybox ]; then
    export $ALIAS="/system/xbin/busybox $COMMAND"
  else if [ -x /sbin/busybox ]; then
    export $ALIAS="/sbin/busybox $COMMAND"
  else
    export $ALIAS="/system/bin/busybox $COMMAND"
  fi; fi; fi; fi; fi
}

alias_cmd()
{
  export_cmd "BUSYBOX" "busybox"
  export_cmd "MOUNT" "mount"
  export_cmd "RM" "rm"
  export_cmd "CP" "cp"
  export_cmd "KILL" "kill"
  export_cmd "KILLALL" "killall"
  export_cmd "GREP" "grep"
  export_cmd "CHMOD" "chmod"
  export_cmd "CHOWN" "chown"
  export_cmd "LN" "ln"
  export_cmd "MKDIR" "mkdir"
  export_cmd "CUT" "cut"
  export_cmd "TAR" "tar"
  export_cmd "SYNC" "sync"
  export_cmd "SLEEP" "sleep"
}

alias_cmd

#### end alias command ############################################

$MOUNT -o remount,rw rootfs /
$CP -f busybox /sbin/busybox
$CHMOD 755 /sbin/busybox

alias_cmd

echo 'Install now....'
$MKDIR /bootmenu
$CP -r /sdcard/bootmenu/* /bootmenu
$MOUNT -o remount,rw /dev/block/mmcblk1p21 /system


$RM -r /system/bootmenu/binary
$RM -r /system/bootmenu/config
$RM -r /system/bootmenu/images
$RM -r /system/bootmenu/script

$MKDIR /system/bootmenu

$TAR xvzpf bootmenu*.tar.gz -C /


$CHMOD 755 /system/bootmenu/binary/*
$CHMOD 755 /system/bootmenu/script/*
$CHMOD 755 /system/bootmenu/recovery/sbin/*
$CHOWN -R 0.0 /system/bootmenu/*

$RM /system/bin/bootmenu
$RM /system/bin/logwrapper
$RM /system/bin/logwrapper.bin

$CP /system/bootmenu/binary/bootmenu /system/bin/
$CP /system/bootmenu/binary/logwrapper.bin /system/bin/
$LN -s /system/bin/bootmenu /system/bin/logwrapper

$CHMOD 755 /system/bin/bootmenu
$CHMOD 755 /system/bin/logwrapper.bin

$RM -r /bootmenu

$SYNC

$MOUNT -o remount,ro /dev/block/mmcblk1p21 /system
$MOUNT -o remount,ro rootfs /

echo
echo 'Install complate....'

exit 0

