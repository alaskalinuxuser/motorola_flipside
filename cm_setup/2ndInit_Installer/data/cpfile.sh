#!/system/bin/sh

echo "==== cp and run install files"
cp -fR /mnt/sdcard/bootmenu /data/bootmenu
cd /data/bootmenu
chmod 755 *
./install.sh

exit 0
