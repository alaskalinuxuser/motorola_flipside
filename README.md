# Motorola Flipside

Here is a collection of stuff I had for the old MB508 Motorola Flipside.

Included is all the source to build it. Android 2.1

Also included are numerous apps that I found particularly useful at that time, which may be difficult to obtain now. Like the WiFi tethering program in a version that still works with this phone.

I've also included the old CyanogenMod 7.2 (I didn't build it) and the special flash files needed to load CWM recovery on the phone. There is also the original Motorola file and program to flash the phone back to stock. Don't know if anyone needs it, but here it is.
