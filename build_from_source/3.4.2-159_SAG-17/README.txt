1. Download following components from source.android.com (TAG android-2.2.1_r1)
to your workspace
   
* dalvik
   * external/expat
   * external/fdlibm
   * external/freetype
   * external/giflib
   * external/icu4c
   * external/jpeg
   * external/libpng
   * external/safe-iop
   * external/skia
   * external/sonivox
   * external/speex
   * external/sqlite
   * external/tagsoup
   * external/tremolo
   * external/wpa_supplicant_6
   * hardware/libhardware
   * hardware/libhardware_legacy
   * frameworks/base
   * frameworks/opt/emoji
   * prebuilt



2. Unpack all archive files downloaded from the site to your workspace

3. Set env variables export MYTOPDIR=<top dir of your workspace>
   cd $MYTOPDIR
   . build/envsetup.sh
   lunch generic-eng
   export TARGET_BUILD_TYPE=release



4. To build kernel:
MOTO_PREBUILT_DIR=$HOME/my-prebuilt
mkdir -p $MOTO_PREBUILT_DIR
make MOTO_PREBUILT_DIR=$MOTO_PREBUILT_DIR kernel


5. To build userspace portions (see bottom for list of supported targets):
make TARGET_ARCH_VARIANT=armv7-a BOARD_HAVE_BLUETOOTH=true BOARD_GPS_LIBRARIES= \
    HARDWARE_OMX=true TARGET_BOARD_PLATFORM=omap3 OMX_TI_OMAP_TIER_LEVEL=10 <target>

Supported targets:

kernel out/target/product/generic/lbl 
out/target/product/generic/system/bin/bluetoothd 
out/target/product/generic/system/bin/btcmd 
out/target/product/generic/system/bin/bthelp 
out/target/product/generic/system/bin/dnsmasq 
out/target/product/generic/system/bin/dumpe2fs 
out/target/product/generic/system/bin/dund 
out/target/product/generic/system/bin/e2fsck 
out/target/product/generic/system/bin/fdisk 
out/target/product/generic/system/bin/hciattach 
out/target/product/generic/system/bin/iptables 
out/target/product/generic/system/bin/mke2fs 
out/target/product/generic/system/bin/mke2fs_static 
out/target/product/generic/system/bin/pand 
out/target/product/generic/system/bin/pppd 
out/target/product/generic/system/bin/resize2fs 
out/target/product/generic/system/bin/sdptool 
out/target/product/generic/system/bin/tc 
out/target/product/generic/system/bin/tune2fs 
out/target/product/generic/system/framework/core.jar 
out/target/product/generic/system/lib/bluez-plugin/audio.so 
out/target/product/generic/system/lib/bluez-plugin/input.so 
out/target/product/generic/system/lib/liba2dp.so 
out/target/product/generic/system/lib/libbluetoothd.so 
out/target/product/generic/system/lib/libbluetooth.so 
out/target/product/generic/system/lib/libbridge.so 
out/target/product/generic/system/lib/libext2_blkid.so 
out/target/product/generic/system/lib/libext2_com_err.so 
out/target/product/generic/system/lib/libext2_e2p.so 
out/target/product/generic/system/lib/libext2fs.so 
out/target/product/generic/system/lib/libext2_profile.so 
out/target/product/generic/system/lib/libext2_uuid.so 
out/target/product/generic/system/lib/libiprouteutil.so 
out/target/product/generic/system/lib/libLCML.so 
out/target/product/generic/system/lib/libnetlink.so 
out/target/product/generic/system/lib/libwbxmlparser.so 
out/target/product/generic/system/lib/ulogd_BASE.so 
out/target/product/generic/system/lib/ulogd_SQLITE3.so